#ifndef __MAIN_SCHEDULES_H_
#define __MAIN_SCHEDULES_H_

#include <omnetpp.h>

using namespace omnetpp;

namespace tsn {


    class Schedules : public cSimpleModule{

    protected:
        virtual void initialize() override;

        virtual void handleMessage(cMessage *msg) override;

    };

}

#endif
