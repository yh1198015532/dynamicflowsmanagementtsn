//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef TSN_COMMON_COMPONENTS_RECORDER_H_
#define TSN_COMMON_COMPONENTS_RECORDER_H_

#include <omnetpp.h>

#include "inet/common/packet/Packet.h"
#include "inet/linklayer/ethernet/EtherFrame_m.h"
#include "inet/linklayer/ieee8021q/Ieee8021qHeader_m.h"
#include "inet/common/TimeTag_m.h"

using namespace omnetpp;
using namespace inet;

namespace tsn{

class Recorder : public cSimpleModule{
private:
    std::string flowName;
    int flowIndex;
    cOutVector E2EVector;
    cDoubleHistogram E2EHistogram;
    bool named = false;
    cOutVector jitter;
    std::string recordName;

    void initialize(int stage) override;
    double computeMaxDelay(Packet* packet);
public:
    std::string getFlowName(){
        return flowName;
    }
    int getFlowIndex(){
        return flowIndex;
    }
    void setFlowName(std::string name){
        flowName = name;
    }
    void setFlowIndex(int i){
        flowIndex = i;
    }
    std::string getRecordName(){
            return recordName;
        }
    void packetArrived(std::string, int subscheduleId, Packet *msg);
    double getAbsJitter();
};

}

#endif /* TSN_COMMON_COMPONENTS_RECORDER_H_ */
