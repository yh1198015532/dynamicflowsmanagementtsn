//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "Dispatcher.h"

namespace tsn{

Define_Module(tsn::Dispatcher);

Dispatcher::Dispatcher() {
    // TODO Auto-generated constructor stub

}

Dispatcher::~Dispatcher() {
    // TODO Auto-generated destructor stub
}

/*cGate *handleMessage(Message *request, cGate *inGate){
    //cGate* down = gate("out",0);
    if(inGate){
        //send(msg, "out", 1);
        return gate("out", 1);
    }else{
        //send(msg, "out", 0);
        return gate("out", 0);
    }

}*/
void Dispatcher::arrived(cMessage *message, cGate *inGate, simtime_t t)
{
    Enter_Method_Silent();
    cGate *outGate = nullptr;
    if (message->isPacket())
        outGate = handlePacket(check_and_cast<Packet *>(message), inGate);
    else
        outGate = handleMessage(check_and_cast<Message *>(message), inGate);
    outGate->deliver(message, t);
}

}
