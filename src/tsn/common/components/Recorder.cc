//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "Recorder.h"

namespace tsn{

Define_Module(tsn::Recorder);

void Recorder::initialize(int stage){
    flowName="";
    flowIndex=-1;
    recordName="";
}

void Recorder::packetArrived(std::string sender, int subscheduleId, Packet *msg){

    if(!named){
        recordName = sender + "-" + std::to_string(subscheduleId);
        std::string s = "E2E-" + recordName;
        E2EVector.setName(s.c_str());
        E2EVector.setUnit("ms");
        s = "jitter-" + recordName;
        jitter.setName(s.c_str());
        jitter.setUnit("ms");
        s = "E2EHistogram-" + recordName;
        E2EHistogram.setName(s.c_str());
        named=true;
    }

    double creationTime;
    auto data = msg->peekData(); // get all data from the packet
    auto regions = data->getAllTags<CreationTimeTag>(); // get all tag regions
    for (auto& region : regions) { // for each region do
        creationTime = region.getTag()->getCreationTime().dbl()*1000; // original time
    }

    //double creation = msg->getCreationTime().dbl()*1000;
    double arrival = msg->getArrivalTime().dbl()*1000;
    double e2eDelay = arrival - creationTime;
    E2EVector.record(e2eDelay);
    E2EHistogram.collect(e2eDelay);

    double maxDelay = computeMaxDelay(msg);
    double jit = e2eDelay-maxDelay;
    jitter.record(jit);
}

double Recorder::computeMaxDelay(Packet* packet){
    std::string name = packet->getName();
    std::string delimiter = "/";
    std::string pcp = name.substr(name.find(delimiter)+1);
    int pcpValue = stoi(pcp);

    double maxDelay=0;
    if(pcpValue == 7 || pcpValue == 3){

        delimiter = ":";
        std::string sSendInterval = name.substr(name.find(delimiter)+1, name.find("/")-name.find(delimiter));
        maxDelay = stod(sSendInterval, nullptr)*1000;
    }else if(pcpValue == 6 || pcpValue == 2){
        maxDelay = 2;
    }else{
        maxDelay = 50;
    }
    return maxDelay;
}

double Recorder::getAbsJitter(){
    double a =E2EHistogram.getMax();
    double b=E2EHistogram.getMin();
    return a-b;
    //return E2EHistogram.getMax()-E2EHistogram.getMin();
}


}
