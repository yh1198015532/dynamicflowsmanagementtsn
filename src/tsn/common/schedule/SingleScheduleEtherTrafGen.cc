//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "SingleScheduleEtherTrafGen.h"

namespace tsn {

Define_Module(SingleScheduleEtherTrafGen);

void SingleScheduleEtherTrafGen::initialize(int stage){

    if (stage == INITSTAGE_LOCAL) {
        arrivalSignal = registerSignal("arrivalSignal");

        startTime = par("startTime").doubleValue();
        offset = par("offset").doubleValue();
        sendInterval = par("sendInterval").doubleValue();
        burstLength = par("numPacketsPerBurst").intValue();
        packetLength = par("packetLength"). intValue();
        pcp = par("pcp").intValue();

    }


    MacAddress destMacAddress;
    const char *destAddress = par("destMacAddress");
    if (destAddress[0]) {
        if (!destMacAddress.tryParse(destAddress))
            destMacAddress = L3AddressResolver().resolve(destAddress, L3AddressResolver::ADDR_MAC).toMac();
    }
    destMacFlow = destMacAddress;



}



}
