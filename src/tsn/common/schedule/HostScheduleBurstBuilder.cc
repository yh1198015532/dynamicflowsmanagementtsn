/*
 * HostScheduleBurstBuilder.cpp
 *
 *  Created on: Jan 21, 2021
 *      Author: sebastiano
 */

#include "../../common/schedule/HostScheduleBurstBuilder.h"

namespace tsn{

HostScheduleBurst<Ieee8021QCtrl>* HostScheduleBurstBuilder::createHostScheduleFromXML(
        cXMLElement *xml, cXMLElement *rootXml) {
    HostScheduleBurst<Ieee8021QCtrl>* schedule = new HostScheduleBurst<Ieee8021QCtrl>();

    // extract cycle time of host
    simtime_t cycle = simTime().parse(
            xml->getFirstChildWithTag("cycle")->getNodeValue());
    schedule->setCycle(cycle);

    std::vector<cXMLElement*> entries = xml->getChildrenByTagName("entry");
    for (cXMLElement* entry : entries) {
        // Get time
        const char* timeCString =
                entry->getFirstChildWithTag("start")->getNodeValue();
        simtime_t time = simTime().parse(timeCString);
        if (time > cycle) {
            throw cRuntimeError("Frame is scheduled after its host cycle ends!");
        }
        // Get size
        const char* sizeCString = entry->getFirstChildWithTag("size")->getNodeValue();
        unsigned size = static_cast<unsigned>(std::ceil(cNedValue::parseQuantity(sizeCString, "B")));

        // Get number of packets in the flow
        const char* numberOfPacketsCString = entry->getFirstChildWithTag("numberOfPackets")->getNodeValue();
        unsigned numberOfPackets = atoi(numberOfPacketsCString);

        // Get Ieee8021QCtrl
        Ieee8021QCtrl header;
        header.q1Tag = VLANTagReq();
        header.macTag = inet::MacAddressReq();
        const char* queueCString =
                entry->getFirstChildWithTag("queue")->getNodeValue();
        header.q1Tag.setPcp(atoi(queueCString));

        const char* addressCString =
                entry->getFirstChildWithTag("dest")->getNodeValue();

        inet::MacAddress destination = inet::MacAddress(addressCString);
        header.macTag.setDestAddress(destination);
        // etherctrl.setTagged(true); no tagged in Ieee802_1QHeader
        header.q1Tag.setVID(0);
        header.q1Tag.setDe(false);

        const char* flowIdCString = entry->getFirstChildWithTag("flowId")->getNodeValue();
        header.flowId = atoi(flowIdCString);

        schedule->addEntry(time, size, header, numberOfPackets);
    }

    return schedule;
}

}
