//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef TSN_APPLICATION_ETHERNET_VLANETHERTRAFGENMULTISCHEDULE_H_
#define TSN_APPLICATION_ETHERNET_VLANETHERTRAFGENMULTISCHEDULE_H_

#include "nesting/application/ethernet/VlanEtherTrafGen.h"
#include "../../common/schedule/SingleScheduleEtherTrafGen.h"
#include <omnetpp.h>
#include <algorithm>
#include "inet/common/packet/Packet.h"
#include "inet/linklayer/common/MacAddress.h"
#include "inet/linklayer/ieee8022/Ieee8022LlcSocket.h"
#include "inet/linklayer/ieee8022/Ieee8022LlcSocketCommand_m.h"
#include "inet/common/ModuleAccess.h"
#include "inet/common/ProtocolTag_m.h"
#include "inet/common/TimeTag_m.h"
#include "inet/common/lifecycle/ModuleOperations.h"
#include "inet/common/packet/chunk/ByteCountChunk.h"
#include "inet/linklayer/common/Ieee802Ctrl.h"
#include "inet/linklayer/common/Ieee802SapTag_m.h"
#include "inet/linklayer/common/MacAddressTag_m.h"
#include "inet/networklayer/common/L3AddressResolver.h"

using namespace omnetpp;
using namespace nesting;

namespace tsn {

class VlanEtherTrafGenMultischedule : public VlanEtherTrafGen{
public:
    VlanEtherTrafGenMultischedule();
    virtual ~VlanEtherTrafGenMultischedule();
protected:
    cPar* pcp = nullptr;
    std::vector<SingleScheduleEtherTrafGen*> schedules;

    virtual void initialize(int stage) override;
    void handleMessageWhenUp(cMessage *msg) override;
    void sendBurstPackets(int n, long len, long portControlProtocol);
    bool isGenerator() override;

};

}

#endif /* TSN_APPLICATION_ETHERNET_VLANETHERTRAFGENMULTISCHEDULE_H_ */
