//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "VlanEtherTrafGenMultischedule.h"



namespace tsn{

Define_Module(VlanEtherTrafGenMultischedule);

VlanEtherTrafGenMultischedule::VlanEtherTrafGenMultischedule() {
    // TODO Auto-generated constructor stub

}

VlanEtherTrafGenMultischedule::~VlanEtherTrafGenMultischedule() {
    // TODO Auto-generated destructor stub
}

void VlanEtherTrafGenMultischedule::initialize(int stage){
    VlanEtherTrafGen::initialize(stage);

    pcp = &par("pcp");

    if(stage == INITSTAGE_APPLICATION_LAYER){
        cModule* scheduleModule = getSubmodule("subSchedule",0);
        if(scheduleModule!=nullptr){
            cModule::SubmoduleIterator it = cModule::SubmoduleIterator(scheduleModule->getParentModule());
            for (; !it.end(); it++) {
                cModule* subModule = *it;
                if (subModule->isName(scheduleModule->getName())) {
                    SingleScheduleEtherTrafGen* schedule = check_and_cast<SingleScheduleEtherTrafGen*>(subModule);
                    schedules.push_back(schedule);
                }
            }

            //initialize parameters EtherTrafGen

            std::size_t i = 0;
            unsigned long int gcdCycle = schedules[i]->getSendInterval()*1000000000000;
            for(i=1; i < schedules.size(); ++i) {
                unsigned long int interval = static_cast<unsigned long int>(schedules[i]->getSendInterval()*1000000000000);
                gcdCycle = std::__gcd(interval ,gcdCycle);
            }

            //par("sendInterval") = ;
            *sendInterval = (double)gcdCycle/1000000000000;
        }
    }


}

void VlanEtherTrafGenMultischedule::handleMessageWhenUp(cMessage *msg)
{
    if (msg->isSelfMessage()) {
        if (msg->getKind() == START) {
            llcSocket.open(-1, ssap);
            /*destMacAddress = resolveDestMacAddress();
            // if no dest address given, nothing to do
            if (destMacAddress.isUnspecified())
                return;
                */
        }
        //inserire ciclo che controlla ogni gcdCycleTime se tocca a un'entry
        size_t size = schedules.size();
        for(std::size_t i = 0; i < size; ++i) {
            unsigned long int time = simTime().inUnit(SIMTIME_PS);
            unsigned long int interval = schedules[i]->getSendInterval()*1000000000000;
            if(interval!=0){
                if(time % interval ==0 && simTime() >= schedules[i]->getStartTime()){
                    destMacAddress = schedules[i]->getDestMacFlow();
                    sendBurstPackets(schedules[i]->getBurstLength(), schedules[i]->getPacketLength(), schedules[i]->getPcp());
                }
            }
        }

        scheduleNextPacket(simTime());
    }
    else
        receivePacket(check_and_cast<Packet *>(msg));
}

void VlanEtherTrafGenMultischedule::sendBurstPackets(int n, long len, long portControlProtocol) {

    *numPacketsPerBurst = n;
    *packetLength = len;
    *pcp = portControlProtocol;
    VlanEtherTrafGen::sendBurstPackets();

    /*int n = numPacketsPerBurst->intValue();
    for (int i = 0; i < n; i++) {
        seqNum++;

        char msgname[40];
        sprintf(msgname, "pk-%d-%ld", getId(), seqNum);

        // create new packet
        Packet *datapacket = new Packet(msgname, IEEE802CTRL_DATA);
        long len = packetLength->intValue();
        const auto& payload = makeShared<ByteCountChunk>(B(len));
        // set creation time
        auto timeTag = payload->addTag<CreationTimeTag>();
        timeTag->setCreationTime(simTime());

        datapacket->insertAtBack(payload);
        datapacket->removeTagIfPresent<PacketProtocolTag>();
        datapacket->addTagIfAbsent<PacketProtocolTag>()->setProtocol(VlanEtherTrafGenSched::L2_PROTOCOL);
        // TODO check which protocol to insert
        auto sapTag = datapacket->addTagIfAbsent<Ieee802SapReq>();
        sapTag->setSsap(ssap);
        sapTag->setDsap(dsap);

        // create control info for encap modules
        auto macTag = datapacket->addTag<MacAddressReq>();
        macTag->setDestAddress(destMacAddress);

        // create VLAN control info
        if (vlanTagEnabled->boolValue()) {
            EnhancedVlanReq* vlanReq = datapacket->addTag<EnhancedVlanReq>();
            vlanReq->setPcp(pcp->intValue());
            vlanReq->setDe(dei->boolValue());
            vlanReq->setVlanId(vid->intValue());
        }

        EV_TRACE << getFullPath() << ": Send packet `" << datapacket->getName()
                        << "' dest=" << macTag->getDestAddress() << " length="
                        << datapacket->getBitLength() << "B type="
                        << IEEE802CTRL_DATA << " vlan-tagged="
                        << vlanTagEnabled->boolValue();
        if (vlanTagEnabled->boolValue()) {
            EV_TRACE << " pcp=" << pcp->intValue() << " dei=" << dei->boolValue() << " vid=" << vid->intValue();
        }
        EV_TRACE << endl;

        emit(packetSentSignal, datapacket);
        send(datapacket, "out");
        packetsSent++;
    }*/
}

bool VlanEtherTrafGenMultischedule::isGenerator()
{
    int n = par("numberOfFlows");
    return n==0 ? false : true;
}

}
