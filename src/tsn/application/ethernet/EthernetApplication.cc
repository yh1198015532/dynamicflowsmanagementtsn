//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "EthernetApplication.h"

namespace tsn {

Define_Module(tsn::EthernetApplication);

const Protocol* EthernetApplication::L2_PROTOCOL = &Protocol::nextHopForwarding;

void EthernetApplication::initialize(int stage) {

    if(stage == INITSTAGE_APPLICATION_LAYER){

        cModule* scheduleModule = getSubmodule("subSchedule",0);
        if(scheduleModule!=nullptr){
            cModule::SubmoduleIterator it = cModule::SubmoduleIterator(scheduleModule->getParentModule());
            for (; !it.end(); it++) {
                cModule* subModule = *it;
                if (subModule->isName(scheduleModule->getName())) {
                    SingleScheduleEtherTrafGen* schedule = check_and_cast<SingleScheduleEtherTrafGen*>(subModule);
                    schedules.push_back(schedule);
                }
            }

            //initialize parameters EtherTrafGen

            std::size_t i = 0;
            unsigned long int gcdCycle = schedules[i]->getSendInterval()*1000000000000;
            for(i=1; i < schedules.size(); ++i) {
                unsigned long int interval = static_cast<unsigned long int>(schedules[i]->getSendInterval()*1000000000000);
                gcdCycle = std::__gcd(interval ,gcdCycle);
            }


            *sendInterval = (double)gcdCycle/1000000000000;
        }
    }

    EtherTrafGen::initialize(stage);

    if (stage == INITSTAGE_LOCAL) {

        //arrivalSignal = registerSignal("arrivalSignal");

        vlanTagEnabled = &par("vlanTagEnabled");
        pcp = &par("pcp");
        dei = &par("dei");
        vid = &par("vid");
    } else if (stage == INITSTAGE_LINK_LAYER) {
        registerService(*L2_PROTOCOL, nullptr, gate("in"));
        registerProtocol(*L2_PROTOCOL, gate("out"), nullptr);
    }


}

void EthernetApplication::handleMessageWhenUp(cMessage *msg)
{
    if (msg->isSelfMessage()) {
        if (msg->getKind() == START) {
            llcSocket.open(-1, ssap);

        }
        //inserire ciclo che controlla ogni gcdCycleTime se tocca a un'entry
        size_t size = schedules.size();
        for(std::size_t i = 0; i < size; ++i) {
            unsigned long int time = simTime().inUnit(SIMTIME_PS);
            unsigned long int interval = schedules[i]->getSendInterval()*1000000000000;
            if(interval!=0){
                double startTime = schedules[i]->getStartTime();
                double offset = schedules[i]->getOffset();
                if(time % interval ==0 && simTime() >= SimTime(startTime)){
                    destMacAddress = schedules[i]->getDestMacFlow();
                    *numPacketsPerBurst = schedules[i]->getBurstLength();
                    *packetLength = schedules[i]->getPacketLength();
                    *pcp = schedules[i]->getPcp();
                    flowIndex = schedules[i]->getIndex();
                    sendBurstPackets(schedules[i]->getSendInterval(), offset);
                }
            }
        }

        scheduleNextPacket(simTime());
    }
    else
        receivePacket(check_and_cast<Packet *>(msg));
}


void EthernetApplication::sendBurstPackets(double sendInterval, double offset) {
    int n = numPacketsPerBurst->intValue();
    //simtime_t delayBase = SimTime(125, SIMTIME_US);
    //simtime_t delayBase = computeDelayBase(startTime);
    simtime_t delay = SimTime(0);

    for (int i = 0; i < n; i++) {
        //delay = delayBase*i;
        delay = computeDelay(offset, i);


        seqNum++;

        char msgname[40];
        sprintf(msgname, "pk-%s-%d-%ld:%f/%d", getOwner()->getFullName(), flowIndex, seqNum, sendInterval, pcp->intValue());

        // create new packet
        Packet *datapacket = new Packet(msgname, IEEE802CTRL_DATA);
        long len = packetLength->intValue();
        const auto& payload = makeShared<ByteCountChunk>(B(len));
        // set creation time
        auto timeTag = payload->addTag<CreationTimeTag>();
        timeTag->setCreationTime(simTime()+delay);

        datapacket->insertAtBack(payload);
        datapacket->removeTagIfPresent<PacketProtocolTag>();
        datapacket->addTagIfAbsent<PacketProtocolTag>()->setProtocol(L2_PROTOCOL);
        // TODO check which protocol to insert
        auto sapTag = datapacket->addTagIfAbsent<Ieee802SapReq>();
        sapTag->setSsap(ssap);
        sapTag->setDsap(dsap);

        // create control info for encap modules
        auto macTag = datapacket->addTag<MacAddressReq>();
        macTag->setDestAddress(destMacAddress);



        // create VLAN control info
        if (vlanTagEnabled->boolValue()) {
            EnhancedVlanReq* vlanReq = datapacket->addTag<EnhancedVlanReq>();
            vlanReq->setPcp(pcp->intValue());
            vlanReq->setDe(dei->boolValue());
            vlanReq->setVlanId(vid->intValue());
        }

        EV_TRACE << getFullPath() << ": Send packet `" << datapacket->getName()
                        << "' dest=" << macTag->getDestAddress() << " length="
                        << datapacket->getBitLength() << "B type="
                        << IEEE802CTRL_DATA << " vlan-tagged="
                        << vlanTagEnabled->boolValue();
        if (vlanTagEnabled->boolValue()) {
            EV_TRACE << " pcp=" << pcp->intValue() << " dei=" << dei->boolValue() << " vid=" << vid->intValue();
        }
        EV_TRACE << endl;

        emit(packetSentSignal, datapacket);

        //send(datapacket, "out");

        /*if(pcp->intValue()==4 || pcp->intValue()==1 ){
                send(datapacket,"out");
        }else{
            sendDelayed(datapacket, delay, "out");
        }*/
        sendDelayed(datapacket, delay, "out");
        packetsSent++;
    }
}

simtime_t EthernetApplication::computeDelay(double offset, int i){
    if(pcp->intValue()==7 || pcp->intValue()==3 ){
            return SimTime(offset);
        }
    if(pcp->intValue()==6 || pcp->intValue()==2 ){
        return SimTime(125, SIMTIME_US)*i;
    }
    if(pcp->intValue()==5 || pcp->intValue()==0 ){
        return SimTime(250, SIMTIME_US)*i;
    }
    if(pcp->intValue()==4 || pcp->intValue()==1 ){
            return SimTime(12240, SIMTIME_NS)*i;
        }
}

bool EthernetApplication::isGenerator()
{
    //int n = par("numberOfFlows");
    double cycle = *sendInterval;
    return cycle==0 ? false : true;
}


void EthernetApplication::receivePacket(Packet *msg)
{
    EV_INFO << "Received packet `" << msg->getName() << "' length= " << msg->getByteLength() << "B\n";

    packetsReceived++;

    std::string name(msg->getName());
    std::string delimiter = "-";
    std::string sender = name.substr(3, name.find(delimiter,3)-3);
    std::string sSubscheduleId = name.substr(name.find(delimiter,3)+1, name.find(delimiter,name.find(delimiter,3)+1)-name.find(delimiter,3)-1);
    int subscheduleId=std::stoi(sSubscheduleId);

    cModule* recorderModule = getSubmodule("recorder",0);
    cModule::SubmoduleIterator it = cModule::SubmoduleIterator(recorderModule->getParentModule());

    bool finded = false;
    for (; !it.end(); it++) {
        cModule* subModule = *it;
        if (subModule->isName(recorderModule->getName())) {
            Recorder* recorder = check_and_cast<Recorder*>(subModule);
            if (recorder->getFlowName().compare(sender)==0 && recorder->getFlowIndex()==subscheduleId) {
                    recorder->packetArrived(sender, subscheduleId, msg);
                    finded=true;
                    break;
            }
        }
    }
    if(!finded){
        cModule::SubmoduleIterator it2 = cModule::SubmoduleIterator(recorderModule->getParentModule());
        for (; !it2.end(); it2++) {
            cModule* subModule = *it2;
            if (subModule->isName(recorderModule->getName())) {
                Recorder* recorder = check_and_cast<Recorder*>(subModule);
                if (recorder->getFlowIndex()==-1) {
                    recorder->setFlowName(sender);
                    recorder->setFlowIndex(subscheduleId);
                    recorder->packetArrived(sender, subscheduleId, msg);
                    break;
                }
            }
        }
    }

    emit(packetReceivedSignal, msg);
    delete msg;
}

void EthernetApplication::finish(){
    cModule* recorderModule = getSubmodule("recorder",0);
    cModule::SubmoduleIterator it = cModule::SubmoduleIterator(recorderModule->getParentModule());

    for (; !it.end(); it++) {
        cModule* subModule = *it;
        if (subModule->isName(recorderModule->getName())) {
            Recorder* recorder = check_and_cast<Recorder*>(subModule);
            if(recorder->getRecordName().compare("")!=0){
                std::string s  = "#AbsJitter-" + recorder->getRecordName();
                recordScalar(s.c_str(), recorder->getAbsJitter());
            }
        }
    }
}

}
